<?php

$gstrTitle = 'Create new menu categories';

include $_SERVER["DOCUMENT_ROOT"].'/menu_dleaman/include/allinclude.php';

$name = (isset($_REQUEST["name"]) )? $_REQUEST["name"] : "";
$sequence = (isset($_REQUEST["sequence"])) ? $_REQUEST["sequence"]: '';
$ProcessRequest = isset($_REQUEST["hidProcess"]) ? $_REQUEST["hidProcess"]: 'N';
$blnIsValid = true;
$strValidationMessage = "You have the following errors: ";

if($ProcessRequest == "Y"){

	$category = new clsCategory();
	$category->name = $name;
	$category->sequence = $sequence;

} else {
	$blnIsValid = false;
	$strValidationMessage='';
}

/*Insert To table
*/
if ($blnIsValid){
	$category->Create();
	$name = "";
	$sequence = "";
}

WriteHeader();

?>

<script>
  $(function() {
    $( "input[type=submit], a, button" ).button();
  });
</script>

<div class="menu">
	<!--Make a form which shows the fields that we are going to take as input.-->
	<form>
		<table>
		<tr>
			<td><label>Category Name: </label></td>
			<td><input type = "text" id="name" name="name"  value ="<?php echo $name;?>"/></td>
		</tr>
		<tr>
			<td><label>Sequence Number: </label></td>
			<td>
				<input type = "text" id="sequence" name="sequence" value ="<?php echo $sequence;?>"/>
				<input type = "hidden" id="hidProcess" name="hidProcess" value="Y"/>
			</td>
		<tr>
			<td><input type="submit" /></td>
		</tr>
	</form>
	<?php
		if(!$blnIsValid){
			echo "<div>".$strValidationMessage."</div>";
		}
	?>
</div> <!-- end content div -->
