<?php
$gstrTitle = "This is a specific category.";

include $_SERVER["DOCUMENT_ROOT"].'/menu_dleaman/include/allinclude.php';

$item = new clsItem();
$cat = (isset($_REQUEST["catSelect"]) )? $_REQUEST["catSelect"] : "";

WriteHeader();

if ($cat == null) {
  $item->CreateCatDropDown();
} else {
  $item->PrintCategory($cat);
}


WriteFooter();

?>
