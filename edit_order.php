<?php
$gstrTitle = 'Add items to an existing order';

include $_SERVER["DOCUMENT_ROOT"].'/menu_dleaman/include/allinclude.php';

$flag = (isset($_REQUEST["orderSubmit"])) ? $_REQUEST["orderSubmit"] : "";
$itemQty = (isset($_REQUEST["itemQty"])) ? $_REQUEST["itemQty"] : "0";

$item = new clsItem();
$order = new clsOrder();

WriteHeader();
echo '<div class="menu">';
  if($flag == null) {
    $order->userID = $_REQUEST["userID"];
    $order->BeginOrder();
    $order->CreateOrderTable($itemQty);
  } else {
    $order->orderID = $flag;
    $x=0;
    // While not null add items to the order class
    while(isset($_REQUEST["itemChoice".$x])) {
      $order->items[$x][0] = $_REQUEST["itemChoice".$x];
      $order->items[$x][1] = $_REQUEST["itemChoice".$x."Qty"];
      $x++;
    }
    $order->FinalizeOrder();
  }
echo '</div>';
WriteFooter();

?>
