<?php

include "medoo.php";
include "db/clsItem.php";
include "db/clsOrder.php";
include "db/clsCategory.php";

include $_SERVER["DOCUMENT_ROOT"].'/menu_dleaman/include/connection.php';

//Common Functions for Pages

function WriteHeader(){

	global $gstrTitle;


	echo '<html>'."\n\r";
	echo '<head>'."\n\r";

	echo '	<title>'.$gstrTitle.'</title>';

	echo '  <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/ui-lightness/jquery-ui.css" type="text/css" media="all" />'."\n\r";
	echo '  <link rel="stylesheet" href="/menu_dleaman/include/style.css" type="text/css" />';

	echo '  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>'."\n\r";
	echo '  <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>'."\n\r";


	echo '</head>';
	echo '<body>';

}

function WriteFooter(){
	echo '<br />';
	echo '<footer>';
	echo 'This website designed by Devin Leaman.';
	echo '</footer>';
	echo '</body>';
	echo '</html>';
}

?>
