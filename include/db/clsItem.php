<?php

Class clsItem{

	public $name;
	public $category_id;
	public $price;
	public $description;
	public $image;
	public $is_featured;
	public $quantity;
	public $itemID;

	function Create(){

		$name = $this->name;
		$category_id = $this->category_id;
		$price = $this->price;
		$description = $this->description;
		$image = $this->image;
		$is_featured = $this->is_featured;

		$PDOdb = new medoo();

		$PDOdb->insert("menu_item", [
			"ITEM_NAME" => $this->name,
			"ITEM_CAT" => $this->category_id,
			"ITEM_PRICE" => $this->price,
			"ITEM_DESC" => $this->description,
			"ITEM_IMAGE" => $this->image,
			"ITEM_IS_FEATURED" => $this->is_featured
		]);
	}

	function PrintMenuItems() {
		$cntr = 0;
		$PDOdb = new medoo();
		$data = $PDOdb->select("menu_item", [
			"ITEM_NAME",
			"ITEM_CAT",
			"ITEM_PRICE",
			"ITEM_DESC"
		],[
			"ORDER" => "ITEM_CAT"
		]);

		echo '<div class="menu">';
		foreach($data as $row) {
			if ($row["ITEM_CAT"] != $cntr) {
				// Print category headings
				if ($row["ITEM_CAT"] == 1) {
					echo ' <h1> Appetizers </h1> ';
					echo ' <div class="menuCat"> ';
				} elseif ($row["ITEM_CAT"] == 2) {
					echo ' </div> ';
					echo ' <h1> Entrees </h1> ';
					echo ' <div class="menuCat"> ';
				} elseif ($row["ITEM_CAT"] == 3) {
					echo ' </div> ';
					echo ' <h1> Desserts </h1> ';
					echo ' <div class="menuCat"> ';
				}
			}

			$cntr = $row["ITEM_CAT"];
			echo '<label>'.$row["ITEM_NAME"].' - $'.$row["ITEM_PRICE"].'</label><br />';
			echo '<p>'.$row["ITEM_DESC"].'</p>';
			echo ' <br /><br /> ';
		}
		echo '</div>';
		echo '</div>';
	}

	function CreateCatDropDown() {
		$PDOdb = new medoo();
		$data = $PDOdb->select("menu_cat", [
			"CAT_ID",
			"CAT_NAME"
		]);
		echo ' <div class="menu"> ';
		echo ' <form method="post" action="category_menu.php"> ';
		echo ' <table> ';
		echo ' <tr> ';
		echo ' <th> Pick a category: </th> ';
		echo ' </tr> ';
		echo ' <tr><td> ';
		echo ' <select name="catSelect" id="catSelect"> ';
		foreach($data as $row) {
			echo ' <option value="'.$row["CAT_ID"].'">'.$row["CAT_NAME"].'</option>';
		}
		echo ' </select> ';
		echo ' </td></tr> ';
		echo ' <tr><td> ';
		echo ' <input type="submit" value="Submit" />';
		echo ' </td></tr> ';
		echo ' </form> ';
		echo ' </div> ';
	}

	function PrintCategory($cat) {
		$PDOdb = new medoo();
		$data = $PDOdb->select("menu_item", [
			"ITEM_NAME",
			"ITEM_PRICE",
			"ITEM_DESC"
		],[
			"ITEM_CAT" => $cat
		]);

		echo ' <div class="menu menuCat">';
		if ($cat == 1)
			echo ' <h1> Appetizers </h1> ';
		elseif ($cat == 2)
			echo ' <h1> Entrees </h1> ';
		elseif ($cat == 3)
			echo ' <h1> Desserts </h1> ';
		foreach($data as $row) {
			echo ' <label>'.$row["ITEM_NAME"].' - $'.$row["ITEM_PRICE"].'</label><br />';
			echo ' <p>'.$row["ITEM_DESC"].'</p>';
			echo ' <br /><br /> ';
		}
		echo ' <a href="category_menu.php">Pick another category</a> ';
		$cat = null;
		echo '</div>';
	}
}

?>
