<?php

Class clsOrder{

	public $orderID;
	public $userID;
	public $items;
	public $total = 0;

  function CreateOrderTable($itemQty) {
		$PDOdb = new medoo();
		$items = $PDOdb->select("menu_item", ["ITEM_ID", "ITEM_NAME"]);

		echo '<form method="post" action="edit_order.php">       ';
		echo '  <table>                                                         ';
		echo '    <thead>                                                       ';
		echo '      <tr>                                                        ';
		echo '        <th colspan="4">Add items to the order</th>   					';
		echo '      </tr>                                                       ';
		echo '    </thead>                                                      ';

		for($cntr=0; $cntr<$itemQty; $cntr++) {
  		echo '    <tr>                                                        ';
  		echo '      <td><label>Item '.($cntr+1).':</label></td>                 ';
  		echo '      <td>																											';
			echo '				<select name="itemChoice'.$cntr.'">';
			// Create the options for the item drop down menu.
			foreach($items as $item) {
				echo '<option value="'.$item["ITEM_ID"].'">'.$item["ITEM_NAME"].'</option>';
			}
			echo '				</select>';
			echo '			</td>																											';
			echo '			<td><label>Quantity:</label></td>';
			echo '			<td><input type="number" name="itemChoice'.$cntr.'Qty" /></td>	';
  		echo '    </tr>                                                       ';
		}

		echo '			<tr>';
		echo '				<td><input type="submit" /></td>';
		echo '				<td><input type="hidden" value="'.$this->orderID.'" name="orderSubmit"/></td>';
		echo '			</tr>';
		echo '  </table>';
		echo '</form>';
	}

  function BeginOrder() {
		date_default_timezone_set("America/Indiana/Knox");
		$PDOdb = new medoo();
		$PDOdb->insert("order", [
			"USER_ID" => $this->userID,
			"ORDER_TIMESTAMP" => date("H:i:s m/d/Y")
		]);

		$orderID = $PDOdb->select("order", [
			"ORDER_ID"
		],[
			"ORDER" => 'ORDER_ID DESC',
			"LIMIT" => '1'
		]);

		foreach($orderID as $row) {
			$this->orderID = $row["ORDER_ID"];
		}
	}

	function GetItemPrice($itemID) {
		$PDOdb = new medoo();
		$price = $PDOdb->select("menu_item", ["ITEM_PRICE"], [
			"ITEM_ID" => $itemID,
			"LIMIT" => '1'
		]);

		foreach($price as $row) {
			$price = $row["ITEM_PRICE"];
		}

		return $price;
	}

	function CalcTotal() {
		$PDOdb = new medoo();
		$data = $PDOdb->select("order_items", [
			"ITEM_PRICE",
			"QUANTITY"
		],
		[
			"ORDER_ID" => $this->orderID
		]);
		foreach($data as $datum) {
			$this->total += ($datum["ITEM_PRICE"] * $datum["QUANTITY"]);
		}
		echo '<p id="orderConfirm">Your order has been completed! <br /> Your total is: $'.$this->total.'</p><br />';
		echo '<a href="new_order.php">New Order</a>';
	}

	function FinalizeOrder() {
		date_default_timezone_set("America/Indiana/Knox");
		$PDOdb = new medoo();

		foreach($this->items as $item) {
			if($item[1] < '0') {
				$item[1] = '1';
			}
			$PDOdb->insert("order_items", [
				"ITEM_ID" => $item[0],
				"QUANTITY" => $item[1],
				"ORDER_ID" => $this->orderID,
				"ITEM_PRICE" => $this->GetItemPrice($item[0])
			]);
		}

		$PDOdb->update("order", [
			"COMPLETE_TIMESTAMP" => date("H:i:s m/d/Y")
		],
		[
			"ORDER_ID" => $this->orderID
		]);

		$this->CalcTotal();
		$this->ResetVars();
	}

	function ResetVars() {
		unset($this->orderID);
		unset($this->itemID);
		unset($this->userID);
		unset($this->items);
		unset($this->total);
	}
}

?>
