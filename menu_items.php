<?php

$gstrTitle = 'This is where you add new menu items.';

include $_SERVER["DOCUMENT_ROOT"].'/menu_dleaman/include/allinclude.php';

$name = (isset($_REQUEST["name"]) )? $_REQUEST["name"] : '';
$category_id = (isset($_REQUEST["category_id"])) ? $_REQUEST["category_id"]: '';
$price = (isset($_REQUEST["price"])) ? $_REQUEST["price"]: '';
$description = (isset($_REQUEST["description"])) ? $_REQUEST["description"]: '';
$image = (isset($_REQUEST["image"])) ? $_REQUEST["image"]: '';
$is_featured = (isset($_REQUEST["is_featured"])) ? $_REQUEST["is_featured"]: '';
$ProcessRequest = isset($_REQUEST["hidProcess"]) ? $_REQUEST["hidProcess"]: 'N';
$blnIsValid = true;
$strValidationMessage = "You have the following errors: ";

if($ProcessRequest == "Y"){

	$item = new clsItem();
	$item->name = $name;
	$item->category_id = $category_id;
	$item->price = $price;
	$item->description = $description;
	$item->image = $image;
	$item->is_featured = $is_featured;

} else {

	$blnIsValid = false;
	$strValidationMessage='';
}

/*Insert To table
*/
if ($blnIsValid){
	$item->Create();
	$name = "";
	$category_id = "";
	$price = "";
	$description = "";
	$image = "";
	$is_featured = "";
}

WriteHeader();

?>

<script>
  $(function() {
    $( "input[type=submit], a, button" ).button();
  });
</script>

<div id="content">
	<form>
		<table>
		<tr>
			<td><label>Item Name: </label></td>
			<td><input type = "text" id="name" name="name"  value ="<?php echo $name;?>"/></td>
		</tr>
		<tr>
			<td><label>Category ID: </label></td>
			<td><input type = "text" id="category_id" name="category_id"  value ="<?php echo $category_id;?>"/></td>
		</tr>
		<tr>
			<td><label>Price: </label></td>
			<td><input type = "text" id="price" name="price"  value ="<?php echo $price;?>"/></td>
		</tr>
		<tr>
			<td><label>Description: </label></td>
			<td><input type = "text" id="description" name="description"  value ="<?php echo $description;?>"/></td>
		</tr>
		<tr>
			<td><label>Picture: </label></td>
			<td><input type = "file" id="image" name="image"  value ="<?php echo $image;?>"/></td>
		</tr>
		<tr>
			<td><label>Featured Item? </label></td>
			<td>
				<select name="is_featured">
					<option value="1">Yes</option>
					<option value="0">No</option>
				</select>
				<input type = "hidden" id="hidProcess" name="hidProcess" value="Y"/>
			</td>
		</tr>
		<tr>
			<td><input type="submit"/></td>
		</tr>
	</form>
	<?php
		if(!$blnIsValid){
			echo "<div>".$strValidationMessage."</div>";
		}
	?>
</div>
