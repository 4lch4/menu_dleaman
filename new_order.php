<?php
$gstrTitle = 'Begin a new order here';

include $_SERVER["DOCUMENT_ROOT"].'/menu_dleaman/include/allinclude.php';

WriteHeader();
?>

<div class="menu">
  <form method="post" action="edit_order.php">
    <table>
      <thead>
        <tr>
          <th colspan="2">Begin a new order</th>
        </tr>
      </thead>

      <tbody>
        <tr>
          <td><label>User ID:</label></td>
          <td><input type="text" name="userID" /></td>
        </tr>

        <tr>
          <td><label>Qty of items:</label></td>
          <td><input type="text" name="itemQty" /></td>
        </tr>

        <tr>
          <td class="tdCent" colspan="2"><input type="submit" /></td>
        </tr>
      </tbody>
    </table>
  </form>
</div>

<?php
WriteFooter();
?>
